from selene import have


# def test_can_login_with_valid_credentials(browser):
#     browser.open("/login")
#     browser.element("#usernameOrEmail").set_value("admin")
#     browser.element("#password").set_value("123456")
#     browser.element("form button").click()
#     browser.element("nav > a.navbar-brand").should(have.exact_text("QAGuild"))


def test_can_login_with_valid_credentials_page_obj(app):
    app.login_page() \
        .open() \
        .login_as("admin", "123456")

    app.main_page().brand_name().should(have.exact_text("QAGuild"))